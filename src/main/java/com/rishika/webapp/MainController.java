package com.rishika.webapp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	private final UserRepository repository;

	MainController(UserRepository repository) {
		this.repository = repository;
	}

	@GetMapping("/users")
	List<User> allUsers() {
		return repository.findAll();
	}

	@PostMapping("/users")
	User createUser(@RequestBody User user) {
		return repository.save(user);
	}

	@DeleteMapping("/users/{id}")
	Boolean deleteUser(@PathVariable Long id) {
		User user = getUser(id);
		if (user == null)
			return false;
		Set<Long> friendsId = user.getFriends();
		for (Long userId : friendsId) {
			User friendUser = getUser(userId);
			friendUser.getFriends().remove(id);
			repository.save(friendUser);
		}
		repository.delete(user);
		return true;
	}

	@GetMapping("/users/{id}")
	User getUser(@PathVariable Long id) {
		return repository.findById(id).orElse(null);
	}

	@GetMapping("/relationships/{id}")
	List<User> viewFriends(@PathVariable Long id) {
		List<User> friends = new ArrayList<User>();
		User user = getUser(id);
		if (user == null)
			return friends;
		Set<Long> friendsId = user.getFriends();
		for (Long userId : friendsId)
			friends.add(getUser(userId));
		return friends;
	}

	@PatchMapping("/relationships/{id1}/add/{id2}")
	User addFriend(@PathVariable Long id1, @PathVariable Long id2) {
		if (id1 == id2)
			return null;
		User user1 = getUser(id1);
		User user2 = getUser(id2);
		if (user1 == null || user2 == null)
			return null;
		user1.getFriends().add(id2);
		user2.getFriends().add(id1);
		repository.save(user1);
		repository.save(user2);
		return user1;
	}

	@PatchMapping("/relationships/{id1}/remove/{id2}")
	User removeFriend(@PathVariable Long id1, @PathVariable Long id2) {
		if (id1 == id2)
			return null;
		User user1 = getUser(id1);
		User user2 = getUser(id2);
		if (user1 == null || user2 == null)
			return null;
		user1.getFriends().remove(id2);
		user2.getFriends().remove(id1);
		repository.save(user1);
		repository.save(user2);
		return user1;
	}

	@GetMapping("/users/{k}/DistanceConnections/{id}")
	List<User> getKDistance(@PathVariable Long k, @PathVariable Long id) {
		if (getUser(id) == null || k < 0)
			return null;

		List<User> k_dist_friends = new ArrayList<User>(); // Contains all the friends with k distance
		Set<Long> visited = new HashSet<Long>();
		Long distance = 0L;

		processFriendList(id, distance, k, k_dist_friends, visited);
		return k_dist_friends;
	}

	// Breadth First Search Utility
	void processFriendList(Long id, Long distance, Long k, List<User> k_dist_friends, Set<Long> visited) {
		Queue<UserDistancePair> queue = new LinkedList<UserDistancePair>(); // Id-Distance Pairs

		queue.add(new UserDistancePair(id, distance));
		visited.add(id);

		while (!queue.isEmpty()) {

			UserDistancePair map = queue.poll();
			User user = getUser(map.getId());
			distance = map.getDistance();
			if (distance == k)
				k_dist_friends.add(user);
			else {
				for (Long friend_id : user.getFriends()) {
					if (visited.contains(friend_id))
						continue;
					visited.add(friend_id);
					queue.add(new UserDistancePair(friend_id, distance + 1));
				}
			}

		}
	}

}

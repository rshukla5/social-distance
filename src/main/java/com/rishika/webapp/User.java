package com.rishika.webapp;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Entity
@NoArgsConstructor
public class User {

	private String email;

	@ElementCollection(fetch = FetchType.EAGER)
	private Set<Long> friends = new HashSet<Long>();

	@Id
	@GeneratedValue
	private Long id;

	private String password;

	private String username;

	public User(String username, String email, String password) {
		this.username = username;
		this.email = email;
		this.password = password;
		this.friends = new HashSet<Long>();
	}

}

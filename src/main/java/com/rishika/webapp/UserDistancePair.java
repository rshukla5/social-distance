package com.rishika.webapp;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class UserDistancePair {

	private final Long id;
	private final Long distance;

}

package com.rishika.webapp;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(OrderAnnotation.class)
public class MainControllerTests {

	@Autowired
	private MainController mainController;

	@Test
	@Order(0)
	public void contextLoads() throws Exception {
		assertThat(mainController).isNotNull();
	}

	@Test
	@Order(1)
	public void allUsersTest() throws Exception {
		assertThat(mainController.allUsers().size()).isEqualTo(0);
	}

	@Test
	@Order(2)
	public void createUserTest() throws Exception {
		List<User> createdUsers = new ArrayList<>();
		for (int i = 1; i <= 5; i++) {
			User user = new User("username" + i, "email" + i + "@test.com", "password" + i);
			createdUsers.add(mainController.createUser(user));
		}

		assertThat(createdUsers.size()).isEqualTo(5);

		User lastUser = createdUsers.get(4);
		assertThat(lastUser.getUsername()).as("username4");
		assertThat(lastUser.getEmail()).as("email4@test.com");
		assertThat(lastUser.getPassword()).as("password4");
	}

	@Test
	@Order(3)
	public void addFriendTest() throws Exception {
		User output = mainController.addFriend(1L, 1L);
		output = mainController.addFriend(1L, 10L);
		output = mainController.addFriend(9L, 1L);
		output = mainController.addFriend(1L, 3L);
		output = mainController.addFriend(1L, 4L);
		output = mainController.addFriend(1L, 5L);

		assertThat(output.getFriends().size()).isEqualTo(3);
		assertThat(output.getFriends()).doesNotContainNull().doesNotHaveDuplicates();
		assertThat(output.getFriends()).containsOnly(3L, 4L, 5L);
	}

	@Test
	@Order(4)
	public void deleteUserTest() throws Exception {
		Boolean output1 = mainController.deleteUser(4L);

		assertThat(output1).isEqualTo(true);

		Boolean output2 = mainController.deleteUser(4L);

		assertThat(output2).isEqualTo(false);

		assertThat(mainController.allUsers().size()).isEqualTo(4);
	}

	@Test
	@Order(5)
	public void viewFriendsTest() throws Exception {
		List<User> output = mainController.viewFriends(10L);

		assertThat(output.size()).isEqualTo(0);

		output = mainController.viewFriends(1L);

		assertThat(output.size()).isEqualTo(2);
	}

	@Test
	@Order(6)
	public void removeFriendTest() throws Exception {
		User output = mainController.removeFriend(1L, 1L);
		output = mainController.removeFriend(1L, 10L);
		output = mainController.removeFriend(9L, 1L);
		output = mainController.removeFriend(1L, 5L);

		assertThat(output.getFriends()).containsOnly(3L);
	}

	@Test
	@Order(7)
	public void getKDistanceTest() throws Exception {
		List<User> output = mainController.getKDistance(-1L, 1L);
		output = mainController.getKDistance(2L, 4L);
		output = mainController.getKDistance(10L, 2L);

		mainController.addFriend(1L, 2L);
		mainController.addFriend(2L, 5L);

		output = mainController.getKDistance(2L, 1L);
		assertThat(output.size()).isEqualTo(1);

		output = mainController.getKDistance(1L, 1L);
		assertThat(output.size()).isEqualTo(2);

		mainController.removeFriend(1L, 3L);
		mainController.addFriend(3L, 2L);
		mainController.addFriend(3L, 5L);

		output = mainController.getKDistance(3L, 1L);
		assertThat(output.size()).isEqualTo(0);

	}
}

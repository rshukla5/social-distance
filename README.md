# Social Distance

*REST* based APIs made using *[Spring-boot](https://spring.io/projects/spring-boot) v2.3.4* framework along with **100% lines and branch** JaCoCo coverage. It uses *[JPA](https://spring.io/projects/spring-data-jpa)* configured with *[H2](http://www.h2database.com/html/main.html)* in-memory database. Given this application has no UI, it does contain a *[Postman](https://www.postman.com/)* API collection which covers all examples for both local testing and prod deployment.

## Installation

#### For Local Testing

* Install [Eclipse](https://www.eclipse.org/downloads/) with *[lombok](https://projectlombok.org/download) & springboot plugins*, later one can be found on Eclipse marketplace
* Create a empty Eclipse workspace and using this command inside it

> `git clone https://rshukla5@bitbucket.org/rshukla5/social-distance.git`

* Finally run this application as *Spring Boot App* and the server will start running at `http://localhost:8080`

#### For Prod Deployment

* Install [Tomcat](https://tomcat.apache.org/download-90.cgi) on your PC/server
* Then run the below command at code root *(i.e. pom.xml location)*

> mvnw.cmd clean & mvnw.cmd install # For Windows

* Now goto *Tomcat Manager* in web browser and upload the *war file* generated inside *./target/*

## Usage

The **User** entity has following attributes:
* Username
* Password
* Email
* Id `(auto-generated)`

The API routing for different use-cases are defined below:

1) A user should be able to create his/her profile.
> @PostMapping("/users")

2) A user should be able to delete his/her profile using Id. `(additional)`
> @DeleteMapping("/users/{id}")

3) A user should be able to add another user in his/her friend list.
Friend list is mutual i.e if A is friend of B, then B is also friend of A.
> @PatchMapping("/relationships/{id1}/add/{id2}")

4) A user should be able to remove another user from his/her friend list.
> @PatchMapping("/relationships/{id1}/remove/{id2}")

5) A user should be able to view his/her friend list.
> @GetMapping("/relationships/{id}")

6) One is able view all users. `(additional)`
> @GetMapping("/users")

7) One is able view a particular user using Id. `(additional)`
> @GetMapping("/users/{id}")

8) Given an input integer K, a user should be able to view all connections at distance K from him.
Distance is defined as the minimum steps needed to reach from user A to user B.

	Some examples for more clarity:
	
	a) For example, if A is a friend of B, then distance of B from A is 1.
	
	b) A is friend of B. B is a friend of C. C is a friend of D. Then Distance(A,D) = 3
	
	c) A ⇔ B , B ⇔ C, C ⇔ D, D ⇔ E, C ⇔ E. In this case Distance(A,E) = 3 as E can be reached via C.

> @GetMapping("/users/{k}/DistanceConnections/{id}")

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change
